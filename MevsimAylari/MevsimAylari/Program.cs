﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MevsimAylari
{
    class Program
    {
        static void Main(string[] args)
        {
            //Girilen mevsimlerin aylarını sıralayan Konsol uygulaması.
            //If Else kullanıldı.

            Console.Write("Bir Mevsim Giriniz:");
            string mevsim = Console.ReadLine();
            //mevsim.ToLower()
            //mevsim.ToUpper()
            if (mevsim.ToLower() == "kış")
            {
                Console.WriteLine("Kış -> Aralık, Ocak, Şubat");
            }
            else if (mevsim.ToLower() == "ilkbahar")
            {
                Console.WriteLine("İlkbahar -> Mart, Nisan, Mayıs");
            }
            else if (mevsim.ToLower() == "yaz")
            {
                Console.WriteLine("Yaz -> Haziran, Temmuz, Ağustos");
            }
            else if (mevsim.ToLower() == "sonbahar")
            {
                Console.WriteLine("Sonbahar -> Eylül, Ekim, Kasım");
            }
            else
            {
                Console.WriteLine("Hatalı Giriş!");
            }
            Console.ReadKey();
        }
    }
}
