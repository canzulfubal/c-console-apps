﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileDongusu
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            int sifre = 9999;
            int girdi;
            do
            {
                Console.Write("Şifre Giriniz: ");
                girdi = int.Parse(Console.ReadLine());
            } while (sifre != girdi); //while (sifre != "9999");
            Console.Write("Giriş Başarılı");
            */
            int hak = 0;
            string sifre;
            do
            {
                Console.Write("Şifre Giriniz: ");
                sifre = Console.ReadLine();
                /*
                if (hak == 3)
                {
                    Console.WriteLine("Giriş Hakkınız Doldu");
                    break;
                }
                */
                hak++;
            } while (sifre != "9999" && hak < 3);
            /*
            if (hak >= 3)
                Console.Write("Giriş Hakkınız Doldu");
            else if (sifre == "9999")
                Console.Write("Giriş Başarılı");
            */
            if (sifre == "9999")
                Console.Write("Şifreyi " + hak + ". defada doğru girdiniz.");
            else
                Console.Write("Giriş Hakkınız Doldu");

            Console.ReadKey();
        }
    }
}
