﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BracketMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Sayı Giriniz:");
            int s1 = int.Parse(Console.ReadLine());
            Console.Write("Sayı Giriniz:");
            int s2 = int.Parse(Console.ReadLine());
            int toplam = s1 + s2;
            //Console.WriteLine("Toplam = " + toplam);
            //Console.WriteLine(s1 + " + " + s2 + " = " + (s1 + s2));
            //Console.WriteLine(s1 + " + " + s2 + " = " + toplam);
            //Console.WriteLine("{0} + {1} = {2}",s1,s2,s1+s2);
            Console.WriteLine("{0} + {1} = {2}", s1, s2, toplam);
            Console.ReadKey();
        }
    }
}
