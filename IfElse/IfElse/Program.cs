﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("1. Sayıyı Giriniz: ");
            int S1 = int.Parse(Console.ReadLine());
            Console.Write("2. Sayıyı Giriniz: ");
            int S2 = int.Parse(Console.ReadLine());
            if (S1 > S2)
            {
                Console.WriteLine("1. Sayı Büyük.");
            }
            else if (S2 > S1)
            {
                Console.WriteLine("2. Sayı Büyük.");
            }
            else
            {
                Console.WriteLine("1. ve 2. sayı eşit.");
            }
            Console.ReadKey();
        }
    }
}
