﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DortIslem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("1. Sayı: ");
            int s1 = int.Parse(Console.ReadLine());

            Console.Write("2. Sayı: ");
            int s2 = int.Parse(Console.ReadLine());
            
            Console.WriteLine("Dört İşlem");
            Console.WriteLine("1. Toplama");
            Console.WriteLine("2. Çıkarma");
            Console.WriteLine("3. Çarpma");
            Console.WriteLine("4. Bölme");
            int secim = int.Parse(Console.ReadLine());

            if (secim == 1)
            {
                Console.WriteLine("Seçiminiz: 1. Toplama");
                Console.WriteLine(s1 + s2);
            }
            else if (secim == 2)
            {
                Console.WriteLine("Seçiminiz: 2. Çıkarma");
                Console.WriteLine(s1 - s2);
            }
            else if (secim == 3)
            {
                Console.WriteLine("Seçiminiz: 3. Çarpma");
                Console.WriteLine(s1 * s2);
            }
            else
            {
                Console.WriteLine("Seçiminiz: 4. Bölme");
                Console.WriteLine(s1 / s2);
            }
            Console.ReadKey();
        }
    }
}
