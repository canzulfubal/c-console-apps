﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KDVHesaplama
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.Write("Malın Fiyatı: ");
            int malfiyat = int.Parse(Console.ReadLine());
            Console.WriteLine("KDV Tutarı: " + (malfiyat / 100) * 18);
            Console.ReadKey();
            */
            /*
            Console.Write("Malın Fiyatı: ");
            double malfiyat = double.Parse(Console.ReadLine());
            Console.WriteLine("KDV Tutarı: " + (malfiyat/100)*18);
            Console.ReadKey();
            */
            Console.Write("Malın Fiyatı: ");
            int malfiyat = int.Parse(Console.ReadLine());
            //int KDV = malfiyat * 18 / 100;
            float KDV = malfiyat * 18 / 100f;
            //Console.WriteLine("KDV Tutarı: " + KDV);
            //Console.WriteLine("KDV'li Tutar: " + (malfiyat + KDV));
            Console.WriteLine("Malın Fiyatı: {0} + KDV Tutarı: {1} = KDV'li Tutar: {2}", malfiyat, KDV, malfiyat + KDV);
            Console.ReadKey();
        }
    }
}
