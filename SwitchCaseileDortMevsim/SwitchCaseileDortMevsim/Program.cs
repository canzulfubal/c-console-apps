﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchCaseileDortMevsim
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Bir Mevsim Giriniz:");
            string mevsim = Console.ReadLine();
            switch (mevsim.ToLower())
            {
                case "kış":
                    Console.WriteLine("Kış -> Aralık, Ocak, Şubat");
                    break;
                case "ilkbahar":
                    Console.WriteLine("İlkbahar -> Mart, Nisan, Mayıs");
                    break;
                case "yaz":
                    Console.WriteLine("Yaz -> Haziran, Temmuz, Ağustos");
                    break;
                case "sonbahar":
                    Console.WriteLine("Sonbahar -> Eylül, Ekim, Kasım");
                    break;
                default:
                    Console.WriteLine("Hatalı Giriş!");
                    break;
            }
            Console.ReadKey();
        }
    }
}
