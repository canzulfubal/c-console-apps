﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonemNotuHesaplama
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("1.Notu Giriniz: ");
            int not1 = int.Parse(Console.ReadLine());
            /*
            if (not1 > 100 || not1 < 0)
                Console.WriteLine("Hatalı Not Girişi Yaptınız");
            */
            /*
            while (not1 > 100 || not1 < 0)
            {
                Console.WriteLine("Hatalı Not Girişi Yaptınız");
                Console.Write("1.Notu Giriniz: ");
                not1 = int.Parse(Console.ReadLine());
            }
            */
            Console.Write("2. Notu Giriniz: ");
            int not2 = int.Parse(Console.ReadLine());
            /*
            if (not2 > 100 || not2 < 0)
                Console.WriteLine("Hatalı Not Girişi Yaptınız");
            */
            /*
            while (not2 > 100 || not2 < 0)
            {
                Console.WriteLine("Hatalı Not Girişi Yaptınız");
                Console.Write("2.Notu Giriniz: ");
                not2 = int.Parse(Console.ReadLine());
            }
            */
            /*
            if (not1 < 0 || not1 > 100 || not2 < 0 || not2 > 100)
            {
                Console.WriteLine("Hatalı Not Girişi Yaptınız");
            }
            else
            {
                //ortalama hesabı ve diğer işlemler yapılır
                // bu uygulama gereksiz önce hataya bakmamak daha hızlandırır 
            }
             * 
             * */
            if (not1 >= 0 && not1 <= 100 && not2 >= 0 && not2 <= 100)
            {
                int ort = (not1 + not2) / 2;
                //Console.WriteLine("Ortalama:" + ort);
                /*
                if (ort >= 50)
                {
                    Console.Write(ort + " ortalama ile Geçtiniz");
                }
                else
                    Console.Write(ort + " ortalama ile Kaldınız");
                */
                /*
                if (ort >= 90)
                {
                    Console.Write(ort + " AA ile Geçtiniz");
                }
                else if (ort >= 80)
                {
                    Console.Write(ort + " BA ile Geçtiniz");
                }
                else if (ort >= 70)
                {
                    Console.Write(ort + " BB ile Geçtiniz");
                }
                else if (ort >= 60)
                {
                    Console.Write(ort + " CB ile Geçtiniz");
                }
                else if (ort >= 50)
                {
                    Console.Write(ort + " CC ile Geçtiniz");
                }
                else
                    Console.Write("Kaldınız");
                */
                if (ort >= 50 && ort < 60)
                    Console.Write(ort + " CC ile Geçtiniz");
                else if (ort >= 60 && ort < 70)
                    Console.Write(ort + " CB ile Geçtiniz");
                else if (ort >= 70 && ort < 80)
                    Console.Write(ort + " BB ile Geçtiniz");
                else if (ort >= 80 && ort < 90)
                    Console.Write(ort + " BA ile Geçtiniz");
                else if (ort >= 90 && ort <= 100)
                    Console.Write(ort + " AA ile Geçtiniz");
                else
                    Console.Write("Kaldınız");
            }
            else
                Console.WriteLine("Hatalı Not Girişi Yaptınız");
            Console.ReadKey();
        }
    }
}
