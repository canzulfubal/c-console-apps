﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GelismisKDVHesablama
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ürün Fiyatını Giriniz: ");
            float urunfiyat = float.Parse(Console.ReadLine());
            Console.WriteLine("KDV Türleri:");
            Console.WriteLine("1- Gıda Ürünleri %1");
            Console.WriteLine("2- Sağlık Ürünleri %8");
            Console.WriteLine("3- Diğer Ürünleri %18");
            Console.Write("Seçiminiz: ");
            string secim = Console.ReadLine();

            float KDV = 0; //dışarıda tanımlananlar içeridede geçerli
            float tutar = 0; //dışarıda tanımlananlar içeridede geçerli
            if (secim == "1")
            {
                //float KDV = urunfiyat * 1 / 100; //içeride tanımlanan dışarıda geçerli değil
                //float tutar = urunfiyat + KDV;
                KDV = urunfiyat * 1 / 100;
                //tutar = urunfiyat + KDV; //if else döngü dışına yazıldığında tekrar yazılmasına gerek yok
                //Console.WriteLine("KDV: %1"); // alttaki temiz writeline sayesinde gerek yok
                //Console.WriteLine("Tutar: " + tutar); // alttaki temiz writeline sayesinde gerek yok
                //Console.WriteLine("Ürün Fiyatı: {0}, KDV: {1}, Tutar: {2}", urunfiyat, KDV, tutar);
            }
            else if (secim == "2")
            {
                KDV = urunfiyat * 8 / 100;
                //tutar = urunfiyat + KDV; //if else döngü dışına yazıldığında tekrar yazılmasına gerek yok
                //Console.WriteLine("KDV: %8");
                //Console.WriteLine("Tutar: " + tutar);
                //Console.WriteLine("Ürün Fiyatı: {0}, KDV: {1}, Tutar: {2}", urunfiyat, KDV, tutar);
            }
            else if (secim == "3")
            {
                KDV = urunfiyat * 18 / 100;
                //tutar = urunfiyat + KDV;
                //Console.WriteLine("KDV: %18");
                //Console.WriteLine("Tutar: " + tutar);
                //Console.WriteLine("Ürün Fiyatı: {0}, KDV: {1}, Tutar: {2}", urunfiyat, KDV, tutar);
            }
            else
            {
                Console.WriteLine("Hatalı Seçim Yaptınız.");
                Console.ReadKey();
            }
            tutar = urunfiyat + KDV;
            Console.WriteLine("Ürün Fiyatı: {0}, KDV: {1}, Tutar: {2}", urunfiyat, KDV, tutar);
            Console.ReadKey();

            /*
            switch (secim)
            {
                case "1":
                    float KDV1 = urunfiyat * 1 / 100;
                    float tutar1 = urunfiyat + KDV1;
                    Console.WriteLine("KDV: %1");
                    Console.WriteLine("Tutar: " + tutar1);
                break;
                case "2":
                    float KDV2 = urunfiyat * 8 / 100;
                    float tutar2 = urunfiyat + KDV2;
                    Console.WriteLine("KDV: %8");
                    Console.WriteLine("Tutar: " + tutar2);
                break;
                case "3":
                    float KDV3 = urunfiyat * 18 / 100;
                    float tutar3 = urunfiyat + KDV3;
                    Console.WriteLine("KDV: %18");
                    Console.WriteLine("Tutar: " + tutar3);
                break;
                default:
                    Console.WriteLine("Hatalı Seçim Yaptınız.");
                break;
            }
            Console.ReadKey();
            */

        }
    }
}
