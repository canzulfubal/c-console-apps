﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileDongusu
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            while (i < 10)
            {
                Console.WriteLine(i);
                i = i + 2;
            }
            Console.ReadKey();
        }
    }
}
